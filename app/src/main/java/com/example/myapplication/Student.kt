package com.example.myapplication

data class Student(
    val id: Long,
    val firstname: String,
    val lastname: String
)