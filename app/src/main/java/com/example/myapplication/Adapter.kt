package com.example.myapplication

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class Adapter(
    private val onDeleteClickListener: (id: Long) -> Unit
) : RecyclerView.Adapter<StudentViewHolder>() {

    private val data = mutableListOf<Student>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StudentViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return StudentViewHolder(inflater, parent, onDeleteClickListener)
    }

    override fun onBindViewHolder(holder: StudentViewHolder, position: Int) {
        holder.onBind(data[position])
    }

    override fun getItemCount(): Int =
        data.size

    override fun getItemId(position: Int): Long =
        data[position].id

    fun setItems(list: List<Student>) {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
    }
}