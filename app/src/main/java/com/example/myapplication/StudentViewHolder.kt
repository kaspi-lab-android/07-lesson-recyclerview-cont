package com.example.myapplication

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_student.view.*

class StudentViewHolder(
    inflater: LayoutInflater,
    parent: ViewGroup,
    private val onDeleteClickListener: (id: Long) -> Unit
) : RecyclerView.ViewHolder(inflater.inflate(R.layout.item_student, parent, false)) {

    private val idTextView = itemView.idTextView
    private val firstNameTextView = itemView.firstNameTextView
    private val lastNameTextView = itemView.lastNameTextView
    private val deleteButton = itemView.deleteButton

    fun onBind(student: Student) {
        idTextView.text = "${student.id}"
        firstNameTextView.text = student.firstname
        lastNameTextView.text = student.lastname
        deleteButton.setOnClickListener {
            onDeleteClickListener(student.id)
        }
    }
}