package com.example.myapplication

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.LinearSmoothScroller
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private val TAG = this.javaClass.simpleName
    private var adapter: Adapter? = null
    private var layoutManager: LinearLayoutManager? = null
    private val scrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            Log.d(TAG, "onScrollStateChanged: newState=$newState")
            when (newState) {
                RecyclerView.SCROLL_STATE_DRAGGING -> toolbar.title = "Dragging"
                RecyclerView.SCROLL_STATE_SETTLING -> toolbar.title = "Settling"
                RecyclerView.SCROLL_STATE_IDLE -> {
                    toolbar.title = "Idle"
                    // Ex: Endless scroll
                }
            }
        }

        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            val firstVisibleItem = layoutManager?.findFirstVisibleItemPosition()
            val firstCompletelyVisibleItem = layoutManager?.findFirstCompletelyVisibleItemPosition()
            val lastVisibleItem = layoutManager?.findLastVisibleItemPosition()
            val lastCompletelyVisibleItem =  layoutManager?.findLastCompletelyVisibleItemPosition()
            Log.d(TAG, "onScrolled: dx=$dx, dy=$dy")
            Log.d(TAG, "onScrolled: firstVisibleItem=$firstVisibleItem, firstCompletelyVisibleItem=$firstCompletelyVisibleItem")
            Log.d(TAG, "onScrolled: lastVisibleItem=$lastVisibleItem, lastCompletelyVisibleItem=$lastCompletelyVisibleItem")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupViews()
    }

    private fun setupViews() {
        val studentList = mutableListOf(
            Student(1, "John", "Doe"),
            Student(2, "Calvin", "Smith"),
            Student(3, "Luis", "Ali"),
            Student(4, "Jessie", "James"),
            Student(5, "Brendan", "Campos"),
            Student(6, "Leon", "Stuart"),
            Student(7, "Charlie", "Mcdaniel"),
            Student(8, "Paul", "Osborne"),
            Student(9, "Jake", "Patterson"),
            Student(10, "Sara", "Owen"),
            Student(11, "Mark", "Drake"),
            Student(12, "Adam", "Griffin"),
            Student(13, "Alex", "Goodman")
        )

        toolbar.setOnMenuItemClickListener { item ->
            when(item.itemId) {
                R.id.add -> {
                    val index = studentList.size + 1L
                    studentList.add(0, Student(index, "First$index", "Last$index"))
                    adapter?.setItems(studentList)
                }
            }

            true
        }

        toolbar.setOnClickListener {
            scrollToTop()
        }

        adapter = Adapter(
            onDeleteClickListener = { id: Long ->
                val position = studentList.indexOfFirst { it.id == id }
                if (position > -1) {
                    Log.d(TAG, "Student $id deleted")
                    studentList.removeAt(position)
                    adapter?.setItems(studentList)
                }
            }
        )

        // ID is unique and will not change
        adapter?.setHasStableIds(true)

        // set adapter
        studentRecyclerView.adapter = adapter

        // Set setHasFixedSize attribute recyclerView.setHasFixedSize(true) when
        // the recycler view item do not change sizes of its children at runtime.
        // By doing this, recyclerview won’t request layout whenever data is updated
        // in the recyclerview item and the view will just invalidate by itself.
        studentRecyclerView.setHasFixedSize(true)

        // this is a default animator
        studentRecyclerView.itemAnimator = DefaultItemAnimator()
        // set to null to disable animations
        // studentRecyclerView.itemAnimator = null

        // layout manager
        layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        studentRecyclerView.layoutManager = layoutManager

        // item decoration
        val itemDecoration = StudentItemDecoration(20, 60)
        studentRecyclerView.addItemDecoration(itemDecoration)

        // set items
        adapter?.setItems(studentList)
    }

    override fun onResume() {
        super.onResume()
        // add scroll listener
        studentRecyclerView.addOnScrollListener(scrollListener)
    }

    override fun onPause() {
        // remove scroll listener
        studentRecyclerView.removeOnScrollListener(scrollListener)
        super.onPause()
    }

    private fun scrollToTop() {
        // smooth scroll
        val smoothScroller = object : LinearSmoothScroller(this) {
            override fun getVerticalSnapPreference(): Int =
                SNAP_TO_START
        }
        smoothScroller.targetPosition = 0
        layoutManager?.startSmoothScroll(smoothScroller)

        // regular scroll
        // layoutManager?.scrollToPosition(0)
    }
}
